resource "random_password" "db_passwords" {
  for_each         = toset(var.environments)
  length           = 16
  special          = true
  override_special = "!%()-_=+[]{}<>"
}

resource "postgresql_role" "db_roles" {
  for_each = toset(var.environments)
  name     = each.value
  login    = true
}

resource "postgresql_database" "databases" {
  for_each          = toset(var.environments)
  name              = postgresql_role.db_roles[each.value].name
  owner             = postgresql_role.db_roles[each.value].name
  template          = "template0"
  lc_collate        = "en_US.UTF-8"
  lc_ctype          = "en_US.UTF-8"
  encoding          = "UTF8"
  connection_limit  = -1
  allow_connections = true
}

