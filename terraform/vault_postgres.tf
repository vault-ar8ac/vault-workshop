resource "vault_mount" "db" {
  path = "postgres"
  type = "database"
}

resource "vault_database_secret_backend_connection" "postgres" {
  backend       = vault_mount.db.path
  name          = "postgres"
  allowed_roles = ["dev", "prod"]

  postgresql {
    connection_url = "postgres://${var.postgres_user}:${var.postgres_password}@${var.postgres_host}:5432/dev?sslmode=disable"
  }
  depends_on = [
    postgresql_database.databases
  ]
}

resource "vault_database_secret_backend_role" "dev" {
  backend             = vault_mount.db.path
  name                = "dev"
  db_name             = vault_database_secret_backend_connection.postgres.name
  creation_statements = ["CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT dev TO \"{{name}}\";"]
}

resource "vault_database_secret_backend_role" "prod" {
  backend             = vault_mount.db.path
  name                = "prod"
  db_name             = vault_database_secret_backend_connection.postgres.name
  creation_statements = ["CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; GRANT prod TO \"{{name}}\";"]
}


