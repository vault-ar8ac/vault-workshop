resource "vault_mount" "pki_ca" {
  type = "pki"
  path = "pki"

  default_lease_ttl_seconds = 5256000
  max_lease_ttl_seconds     = 5256000
}

resource "vault_mount" "pki_intermediate" {
  type = "pki"
  path = "pki_int"

  default_lease_ttl_seconds = 2628000
  max_lease_ttl_seconds     = 2628000
}

resource "vault_pki_secret_backend_root_cert" "ca" {
  backend              = vault_mount.pki_ca.path
  type                 = "internal"
  common_name          = "Example Root CA"
  ttl                  = "315360000"
  format               = "pem"
  private_key_format   = "der"
  key_type             = "rsa"
  key_bits             = 4096
  exclude_cn_from_sans = true
  ou                   = "Example org. unit"
  organization         = "Example Corporate"

  depends_on = [
    vault_mount.pki_ca
  ]
}

resource "vault_pki_secret_backend_intermediate_cert_request" "int2022" {
  backend     = vault_mount.pki_intermediate.path
  type        = vault_pki_secret_backend_root_cert.ca.type
  common_name = "Example Corporate Intermediate CA"
}

resource "vault_pki_secret_backend_root_sign_intermediate" "int2022" {
  backend              = vault_mount.pki_ca.path
  csr                  = vault_pki_secret_backend_intermediate_cert_request.int2022.csr
  common_name          = "Example Corporate Intermediate CA 2022"
  exclude_cn_from_sans = true
  ou                   = "Example org. unit"
  organization         = "Example Corporate"
  country              = "CZ"
  locality             = "Pilsen"
  province             = "CZ"
  ttl                  = "2628000"
  revoke               = true
}

resource "vault_pki_secret_backend_intermediate_set_signed" "int2022" {
  backend     = vault_mount.pki_intermediate.path
  certificate = vault_pki_secret_backend_root_sign_intermediate.int2022.certificate
}

resource "vault_pki_secret_backend_role" "example_corp" {
  backend          = vault_mount.pki_intermediate.path
  name             = "example-corp"
  ttl              = 2592000
  allow_ip_sans    = true
  key_type         = "rsa"
  key_bits         = 4096
  allowed_domains  = ["example.com", "example.corp"]
  allow_subdomains = true
}

resource "vault_pki_secret_backend_cert" "app1_example_corp" {
  backend = vault_mount.pki_intermediate.path
  name    = vault_pki_secret_backend_role.example_corp.name

  common_name = "app1.example.corp"

  auto_renew = true
  alt_names = [
    "www.app1.example.corp",
    "app1.example.com",
    "www.app1.example.com"
  ]

  depends_on = [
    vault_pki_secret_backend_role.example_corp
  ]
}
