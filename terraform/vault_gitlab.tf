resource "vault_jwt_auth_backend" "gitlab_jwt" {
  type         = "jwt"
  path         = "gitlab-jwt"
  bound_issuer = "gitlab.com"
  jwks_url     = "https://gitlab.com/-/jwks"
  default_role = "default"
  tune {
    token_type        = "default-service"
    default_lease_ttl = "768h"
    max_lease_ttl     = "768h"
  }
}

resource "vault_jwt_auth_backend_role" "gitlab_jwt_default" {
  backend           = vault_jwt_auth_backend.gitlab_jwt.path
  role_name         = "default"
  role_type         = "jwt"
  token_policies    = ["default"]
  bound_claims_type = "glob"
  user_claim        = "user_email"
  bound_audiences   = ["https://vault.plugin.auth.jwt.test"]
}

resource "vault_jwt_auth_backend_role" "pipeline" {
  backend        = vault_jwt_auth_backend.gitlab_jwt.path
  role_name      = "pipeline"
  token_policies = ["default", "readonly"]

  user_claim        = "user_email"
  bound_claims_type = "glob"
  bound_claims = {
    namespace_path = "vault-ar8ac"
  }
  role_type = "jwt"
}

resource "vault_jwt_auth_backend" "gitlab_oidc" {
  type               = "oidc"
  path               = "oidc"
  bound_issuer       = "https://gitlab.com"
  oidc_discovery_url = "https://gitlab.com"
  oidc_client_id     = var.gitlab_client_id
  oidc_client_secret = var.gitlab_client_secret
  default_role       = "default"
  tune {
    token_type        = "default-service"
    default_lease_ttl = "768h"
    max_lease_ttl     = "768h"
  }
}

resource "vault_jwt_auth_backend_role" "gitlab_oidc_default" {
  backend           = vault_jwt_auth_backend.gitlab_oidc.path
  role_name         = "default"
  role_type         = "oidc"
  token_policies    = ["default"]
  bound_claims_type = "glob"
  user_claim        = "nickname"
  allowed_redirect_uris = [
    "https://${var.vault_domain}/ui/vault/auth/oidc/oidc/callback",
    "https://${var.vault_domain}/oidc/callback",
    "http://localhost:8250/ui/vault/auth/oidc/oidc/callback",
    "http://localhost:8250/oidc/callback",
  ]
}

resource "vault_jwt_auth_backend_role" "gitlab_oidc_admins" {
  backend           = vault_jwt_auth_backend.gitlab_oidc.path
  role_name         = "admin"
  role_type         = "oidc"
  token_policies    = ["default", "admin"]
  bound_claims_type = "glob"
  user_claim        = "nickname"

  groups_claim = "/groups"

  bound_claims = {
    "groups" : "vault-ar8ac"
  }

  allowed_redirect_uris = [
    "https://${var.vault_domain}/ui/vault/auth/oidc/oidc/callback",
    "https://${var.vault_domain}/oidc/callback",
    "http://localhost:8250/ui/vault/auth/oidc/oidc/callback",
    "http://localhost:8250/oidc/callback",
  ]
}

resource "vault_policy" "pipeline" {
  name = "app1-pipeline"

  policy = <<EOT
path "kv/app1/data/dev/*" {
  capabilities = [ "read" ]
}
EOT
}

resource "vault_policy" "admin" {
  name   = "admin"
  policy = <<EOT
path "*" {
  capabilities = ["sudo","read","create","update","delete","list", "patch"]
}
EOT
}
