variable "vault_url" {
  type    = string
  default = "http://localhost:8200"
}

variable "vault_domain" {
  type    = string
  default = "localhost"
}

variable "vault_namespace" {
  type    = string
  default = null
}

variable "login_approle_role_id" {
  type      = string
  sensitive = true
}

variable "login_approle_secret_id" {
  type      = string
  sensitive = true
}

variable "gitlab_client_id" {
  type = string
}

variable "gitlab_client_secret" {
  type = string
}

variable "environments" {
  type    = list(string)
  default = ["dev", "prod"]
}

variable "postgres_host" {
  type = string
}

variable "postgres_user" {
  type = string
}

variable "postgres_password" {
  type      = string
  sensitive = true
}

variable "users_devs" {
  type    = list(string)
  default = []
}

variable "users_admins" {
  type    = list(string)
  default = []
}

variable "users_testers" {
  type    = list(string)
  default = []
}


