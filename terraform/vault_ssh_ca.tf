resource "vault_mount" "ssh" {
  type = "ssh"
  path = "ssh"
}

resource "vault_ssh_secret_backend_ca" "main" {
  backend              = vault_mount.ssh.path
  generate_signing_key = true
}

resource "vault_ssh_secret_backend_role" "admin" {
  name                    = "admin"
  backend                 = vault_mount.ssh.path
  key_type                = "ca"
  allow_user_certificates = true

  allowed_users = "admin"
  default_user  = "admin"

  allowed_extensions = "permit-pty,permit-port-forwarding"
  default_extensions = {
    "permit-pty" = ""
  }
}
